const Blog = require('../models/blog');

exports.getAllBlog = (req, res, next) => {
    //for gettting all the post
    Blog.find()
    .then(blogs => {
        res.status(200).json({message : 'Blogs Fetched SuccessFully..!', blogs : blogs});
    })
    .catch(err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    });   
}

exports.getBlog = (req, res, next) => {
  // for getting single Post
  const blogId = req.params.blogId;
     Blog.findById(blogId)
     .then(blog => {
         if(!blog){
             const error = new Error('Blog not Found!');
             error.statusCode= 404;
             throw error;
         }
         res.status(200)
         .json({ message :'Blog Fetched', blog : blog})
     })
     .catch(err => {
         if(!err.statusCode){
             err.statusCode = 500;
         }
         next(err);
     })
}