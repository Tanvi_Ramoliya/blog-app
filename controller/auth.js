const jwt = require('jsonwebtoken');

const Admin = require('../models/admin');

const { validationResult } = require('express-validator');

const bcrypt = require('bcryptjs');

exports.signup = (req, res , next) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        const error = new Error('validation Failed!');
        error.statusCode = 422;
        throw error;
    }
    const email = req.body.email;
    const password = req.body.password;
    bcrypt.hash(password, 12)
   .then(hashedPass => {
       const admin = new Admin({
           email : email,
           password : hashedPass,
       })
       return admin.save();
   })
   .then(result => {
       res.status(201).json({
           message : 'Admin Created!',
           adminId : result._id
       })
   })
   .catch(err => {
       if(!err.statusCode){
           err.statusCode = 500;
       }
       next(err);
   });
}

exports.login = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    //here, we will check for authorized Admin;

    let loadedAdmin;
    Admin.findOne({email : email})
    .then(admin => {
        if(!admin){
            const error = new Error('A Admin with this email could not be Found!');
            error.statusCode = 401;
            throw error;
        }
        loadedAdmin = admin;
        return bcrypt.compare(password, admin.password);
    })
    .then(isEqual => {
        if(!isEqual){
            const error = new Error('Wrong Password!');
            error.statusCode = 401;
            throw error;
        }
        const token = jwt.sign({
            email : loadedAdmin.email,
            adminId :  loadedAdmin._id.toString()
        }, 'secret' , { expiresIn : '3h'});
        res.status(200).json({
            token : token,
            adminId : loadedAdmin._id.toString()
        })
    })
    .catch(err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    })
}