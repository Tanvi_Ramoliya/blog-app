const fs = require('fs');
const path = require('path');
const { validationResult } = require('express-validator');

const Blog = require('../models/blog');
const Category = require('../models/category');
const Admin = require('../models/admin');

exports.getAllBlogs = (req, res, next) => {
    //getting all the Blogs
    Blog.find()
    .then(blogs => {
        res.status(200).json({message : 'Blogs Fetched SuccessFully..!', blogs : blogs})
    })
    .catch(err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    });   
}   

exports.createBlog = (req, res, next) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        const error = new Error('Validation Failed! entered Data is incorrect!');
        error.statusCode = 422;
       throw error;
    }
    if(!req.file){
        const error = new Error('No Image Provided!');
        error.statusCode = 422;
       throw error;
    }
    const imageUrl = req.file.path.replace("\\" ,"/");
    const title = req.body.title;
    const description = req.body.description;
    const category = req.body.category;
    const blog = new Blog({
        title : title, 
        description : description,
        category : category,
        imageUrl : imageUrl,
    });

    blog.save()
    .then(result => {
        return Admin.findById(req.adminId);
    })
    .then(admin => {
        admin.blogs.push(blog);
        return admin.save();
    })
    .then(result => {
        res.status(201)
        .json({
            message : 'Post successfully created!',
            blog : blog
        })
    })
    .catch(err => {       
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    })      
}
exports.updateBlog = (req, res, next) => {
    const blogId = req.params.blogId;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        const error = new Error('Validation Failed! entered Data is incorrect!');
        error.statusCode = 422;
       throw error;
    }
    const title = req.body.title;
    const description = req.body.description;
    const category = req.body.category;
    let imageUrl = req.body.imageUrl;
    if(req.file){
        imageUrl = req.file.path.replace("\\" ,"/");
    }
    if(!imageUrl){
        const error = new Error('No File Picked!');
        error.statusCode = 422;
        throw error;
    }    
    Blog.findById(blogId)
    .then(blog => {
        if(!blog){
            const error = new Error('Blog not Found!');
            error.statusCode= 404;
            throw error;
        }        
        if(imageUrl !== blog.imageUrl){
            clearImage(blog.imageUrl);
        }
        blog.title = title;
        blog.imageUrl = imageUrl;
        blog.description = description;
        blog.category = category;
        return blog.save();
    })
    .then(result => {
        res.status(200).json({
            message : 'Blog Updated!',
            blog : result
        })
    })
    .catch(err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    });   
}
exports.deleteBlog = (req, res, next) => {
    const blogId = req.params.blogId;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        const error = new Error('Validation Failed! entered Data is incorrect!');
        error.statusCode = 422;
       throw error;
    }
    Blog.findById(blogId)
    .then(blog => {
        if(!blog){
            const error = new Error('Blog not Found!');
            error.statusCode= 404;
            throw error;
        }
        clearImage(blog.imageUrl);
        return Blog.findByIdAndRemove(blogId);
    })
     .then(result => {
      return Admin.findById(req.adminId);
    })
    .then(admin => {
        admin.blogs.pull(req.blogId);
         return admin.save();
    })
    .then(result => {
        res.status(200)
        .json({
            message : 'Blog Deleted!'
        })
    })
    .catch(err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    })   
}
exports.getAllCategories = (req, res, next) => {
    Category.find()
    .then(categories => {
        res.status(200).json({message : 'All Categories Fetched SuccessFully..!', categories : categories})
    })
    .catch(err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    });   
   
}
exports.createCategory = (req, res, next) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        const error = new Error('Please Enter Proper Category!!');
        error.statusCode = 422;
       throw error;
    }
    const name = req.body.name;
    const category = new Category({
        name : name
    });
    category.save()
    .then(result => {
        return Admin.findById(req.adminId);
    })    
    .then(admin => {
        admin.categories.push(category);
        return admin.save();
    })
    .then(result => {
        res.status(201)
        .json({
            message : 'Category successfully Added!',
            category : category
        })
    })
    .catch(err => {       
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    })

   
}
exports.updateCategory = (req, res, next) => {
    const categoryId = req.params.categoryId;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        const error = new Error('Validation Failed! entered Data is incorrect!');
        error.statusCode = 422;
       throw error;
    }
    const name = req.body.name;
    Category.findById(categoryId)
    .then(category => {
        if(!category){
            const error = new Error('Blog not Found!');
            error.statusCode= 404;
            throw error;
        }       
        category.name = name;
        return category.save();
    })
    .then(result => {
        res.status(200).json({
            message : 'Category Updated!',
            category : result
        })
    })
    .catch(err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    });
   
}
exports.deleteCategory = (req, res, next) => {
    const categoryId = req.params.categoryId;
    Category.findById(categoryId)
    .then(category => {
        if(!category){
            const error = new Error('Category not Found!');
            error.statusCode= 404;
            throw error;
        }
        return Category.findByIdAndRemove(categoryId);
    })
    .then(result => {
        return Admin.findById(req.adminId);
      })
      .then(admin => {
          admin.categories.pull(req.categoryId);
           return admin.save();
      })
    .then(result => {
        res.status(200)
        .json({
            message : 'Category Deleted!'
        })
    })
    .catch(err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    })      
}

const clearImage = filepath => {
    filepath = path.join(__dirname , '..' , filepath);
    fs.unlink(filepath, err => {console.log(err)});
 }