const express = require('express');

const router = express.Router();

const AuthController = require('../controller/auth');

const { body } = require('express-validator');

const Admin = require('../models/admin');

router.put('/signup', [
    body('email')
    .isEmail()
    .withMessage('Please enter a valid email.')
    .custom((value , { req }) => {
        return Admin.findOne({ email : value}).then(adminDoc => {
            if(adminDoc){
                return Promise.reject(' E-mail Address already exists!!');
            }
        })
    })
    .normalizeEmail(),
    body('password').trim().isLength({ min : 5}),
], AuthController.signup);

router.post('/login', AuthController.login)

module.exports = router;