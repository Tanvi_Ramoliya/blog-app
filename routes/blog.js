const express = require('express');

const router = express.Router();

const { body } = require('express-validator');

const blogController = require('../controller/blog');

const isAuth = require('../middleware/is-auth');

router.get('/blogs',isAuth, blogController.getAllBlogs);

router.post('/blog', isAuth,[
    body('title').trim().isLength({min : 4}),
    body('description').trim().isLength({ min : 5}),
    body('category').not().isEmpty(),    
],blogController.createBlog);

router.put('/blog/:blogId', isAuth ,[
    body('title').trim().isLength({min : 4}),
    body('description').trim().isLength({ min : 5}),
    body('category').not().isEmpty(),    
], blogController.updateBlog);

router.delete('/blog/:blogId',isAuth , blogController.deleteBlog);

router.get('/categories', isAuth , blogController.getAllCategories);

router.post('/category', isAuth ,[
    body('name').trim().isLength({ min : 2})
], blogController.createCategory);

router.put('/category/:categoryId', isAuth ,[
    body('name').trim().isLength({ min : 2})
], blogController.updateCategory);

router.delete('/category/:categoryId', isAuth , blogController.deleteCategory);


module.exports = router;