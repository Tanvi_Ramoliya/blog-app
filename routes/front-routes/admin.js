const express = require('express');

const router = express.Router();

router.get('/admin/AllBlogs', (req,res,next) => {
    res.render('admin/allblogs');
});
router.get('/admin/add/blog', (req,res,next) => {
    res.render('admin/add-new-blog');
});
router.get('/admin/allcategories', (req,res,next) => {
    res.render('admin/allcategories');
});
router.get('/admin/add/category', (req,res,next) => {
    res.render('admin/add-new-category');
});
router.get('/logout', (req, res, next) => {
        res.redirect('/login');
})

module.exports = router;