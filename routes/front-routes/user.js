const express = require('express');

const router = express.Router();

router.get('/', (req, res,next) => {
  res.render('user/index');
})
router.get('/blogs', (req, res, next) =>{  
    res.render('user/index');  
}); 

router.get('/blog/:blogId' , (req, res, next) => {
    res.render('user/blog-details');
})

module.exports = router;