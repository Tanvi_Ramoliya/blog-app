const express = require('express');

const router = express.Router();

router.get('/login', (req,res,next) => {
  res.render('auth/index'); 
});

router.post('/login/check',(req,res, next) => {
  res.render('auth/check');
})

module.exports = router;