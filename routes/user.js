const express = require('express');

const router = express.Router();

const userController = require('../controller/user');

router.get('/blogs', userController.getAllBlog);

router.get('/blog/:blogId', userController.getBlog);

module.exports = router;