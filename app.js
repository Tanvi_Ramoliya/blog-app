const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');

const app = express();
const fileStorage = multer.diskStorage({
       destination: (req, file, cb) => {
           cb(null, 'images');
       },
       filename: (req, file, cb) => {
           cb(null, Math.random() + '-' + file.originalname  )
       }
    });
    
    const fileFilter = (req, file, cb) => {
       if(file.mimetype === 'image/png' ||
       file.mimetype === 'image/jpg' || 
       file.mimetype === 'image/jpeg'){
           cb(null, true);
       }
       else {
           cb(null, false);
       }
    }

app.set('view engine', 'ejs');
app.set('views', 'views');

const userRoutes = require('./routes/user');

const adminRoutes = require('./routes/blog');

const authRoutes = require('./routes/auth');

const ViewUserRoutes = require('./routes/front-routes/user');
const ViewAdminRoutes = require('./routes/front-routes/admin');
const ViewAuthRoutes = require('./routes/front-routes/auth');

app.use(bodyParser.json()); 

app.use(multer({storage : fileStorage , fileFilter : fileFilter}).single('image'));    
app.use('/images', express.static(path.join(__dirname, 'images')));

app.use((req, res,next) => {
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods','GET, POST,PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'content-Type, Authorization');
    next();
})


app.use(ViewUserRoutes);
app.use(ViewAdminRoutes);
app.use(ViewAuthRoutes);
app.use('/admin', adminRoutes);
app.use('/auth', authRoutes);
app.use('/user',userRoutes);


app.use((error, req, res, next) => {
    const status = error.statusCode  || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({message: message, data : data});

})

mongoose.connect('mongodb+srv://analytics:zzgD5VzzekNO1dSw@mflix.d0qet.mongodb.net/app?retryWrites=true&w=majority',{ useNewUrlParser: true })
.then(result => {
    app.listen(3001);
})
.catch(err => {
    console.log(err);
})

